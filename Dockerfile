ARG ALPINE_IMAGE_VERSION=latest

FROM alpine:${ALPINE_IMAGE_VERSION} AS system-config
RUN apk add --no-cache bash cmake make g++ gcc
SHELL ["bash", "-c"]

FROM system-config AS builder
ARG CMAKE_BUILD_BINARY_TARGET="main"
ARG PROJECT_ID="gitlab-dind-ci"
LABEL stage="builder"
LABEL project="${PROJECT_ID}"
COPY . /gitlab-dind-ci
WORKDIR /gitlab-dind-ci/build/
RUN cmake .. \
    && make "${CMAKE_BUILD_BINARY_TARGET}" -j$(( $(nproc) / 2 + 1 ))

FROM alpine:${ALPINE_IMAGE_VERSION} AS app
ARG CMAKE_BUILD_BINARY_TARGET
ARG PROJECT_ID
LABEL stage="app"
LABEL project="${PROJECT_ID}"
COPY --from=builder "/gitlab-dind-ci/build/bin/${CMAKE_BUILD_BINARY_TARGET}" ./app/
RUN apk add --no-cache bash libstdc++
